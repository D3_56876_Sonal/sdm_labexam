//Import express
const express = require('express')

//Create express application
const app = express()

//JSON parser to parse JSON data sent through request body.
app.use(express.json())

const cors = require("cors");
app.use(cors("*"));

//import student module
const routerMovie = require('./routes/movie')

// /student
app.use('/movie',routerMovie)


//Start Express Application on required port lets say 4000
app.listen(4000, () =>{
    console.log('Server Started on port 4000')
})