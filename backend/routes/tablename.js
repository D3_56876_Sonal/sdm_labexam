const{ query } = require('express')
const express = require ('express')
const router = express.Router()
const utils = require('../utils')
const db = require('../db')

//Add new Movie
router.post('/add', (request, response) => {
    const {movie_id, movie_title, movie_release_date,movie_time,director_name} = request.body
    const statement = `
        insert into movie
        (movie_id, movie_title, movie_release_date,movie_time,director_name)
        values
        (${movie_id},'${movie_title}', '${movie_release_date}',${movie_time}, '${director_name}')
        `
        db.execute(statement, (error, result) => {
            response.send(utils.createResult(error,result));

        })
})

//Display data of movie
router.get('/:movie_title', (request, response) => {
    const{movie_title} = request.params
    const statement = `SELECT * FROM movie where movie_title = '${movie_title}'`
    db.execute(statement, (error, result) => {
        response.send(utils.createResult(error, result))
})
})

//Delete a movie
router.delete('/delete/:id', (request, response) => {
    const{movie_id} = request.params
    const statement = `DELETE FROM movie WHERE movie_id= ${movie_id}`

    db.execute(statement, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

module.exports = router;

//Update a movie
router.put('/update/:movie_time', (request, response) => {
    const {movie_title} = request.params
    const {movie_time} = request.body

    const statement = `UPDATE movie SET movie_time = ${movie_time} where movie_time = '${movie_time}'`

    db.execute(statement, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})
